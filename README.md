# Monster creator and compendium pack importer for Pathfinder 2.

Provides APIs to create NPCs from JSON and import into compendium packs

This module uses trademarks and/or copyrights owned by Paizo Inc., which are used under Paizo's Community Use Policy. We are expressly prohibited from charging you to use or access this content. This module is not published, endorsed, or specifically approved by Paizo Inc. For more information about Paizo's Community Use Policy, please visit paizo.com/communityuse. For more information about Paizo Inc. and Paizo products, please visit paizo.com.

Forked from at https://gitlab.com/unindel/fvtt-pf2e-beastiary-1-compendium-pack-module

### Basic Instructions

This module provides two APIs:
*  createPF2eMonsters(source_json)


Takes a json file as an input parameter that is in the importJSON folder. This file must contain an array of monster objects.

*  importPF2eMonsters(compendium_pack_collection)
  

Imports all actors in the world into the provided compendium pack.

### License

Project Licensing:
*  All Javascript in this project is licensed under the Apache License v2.


Content Usage and Licensing:
*  Any Pathfinder Second Edition information used under the Paizo Inc. Community Use Policy (https://paizo.com/community/communityuse)
*  Game system information and mechanics are licensed under the Open Game License (OPEN GAME LICENSE Version 1.0a).